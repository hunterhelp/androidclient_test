package ru.greentooth.greentoothandroidtest;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import ru.greentooth.greentoothandroidtest.model.Product;
import ru.greentooth.greentoothandroidtest.model.ProductList;
import ru.greentooth.greentoothandroidtest.model.UserTokenModel;
import ru.greentooth.greentoothandroidtest.request.AuthRequest;
import ru.greentooth.greentoothandroidtest.request.PopularProductsRequest;

public class MainActivity extends AppCompatActivity {

//public class MainActivity extends Activity {

    protected SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
    }

    @Override
    protected void onStop()
    {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Делает запрос
     */
    void performRequest()
    {
        PopularProductsRequest request = new PopularProductsRequest();
        spiceManager.execute(request, "test", DurationInMillis.ALWAYS_EXPIRED, new PopularProductsRequestListener());
    }

    void performAuthTestRequest()
    {
        AuthRequest request = new AuthRequest("", "");
        spiceManager.execute(request, "auth", DurationInMillis.ALWAYS_EXPIRED, new AuthRequestListener());
    }


    int sample;
    double sampleRate;
    double duration;
    double time;
    double f1;
    double f2;
    double amplitude1;
    double amplitude2;
    double sineWave1;
    double sineWave2;
    float[] buffer1;
    float[] buffer2;
    byte[] byteBuffer1;
    byte[] byteBuffer2;
    byte[] byteBufferFinal;
    int bufferIndex;
    short x;
    short y;
    AudioTrack audioTrack;

    void testAudio(int monostereo)
    {



        sampleRate = 44100.0;
        duration = 20.0;
        f1 = 440.0;
        amplitude1= 1;
        f2 = 444.0;
        amplitude2 = 0;

        buffer1 = new float[(int)(duration*sampleRate)];
        buffer2 = new float[(int)(duration*sampleRate)];

        for(sample = 0; sample < buffer1.length; sample ++){
            time = sample / sampleRate;
            buffer1[sample] = (float)(amplitude1*Math.sin(2*Math.PI*f1*time));
            buffer2[sample] = (float)(amplitude2*Math.sin(2*Math.PI*f2*time));
        }

        byteBuffer1 = new byte[buffer1.length*2]; //two bytes per audio frame, 16 bits

        for(int i = 0, bufferIndex=0; i < byteBuffer1.length; i++){
            x = (short) (buffer1[bufferIndex++]*32767.0); // [2^16 - 1]/2 = 32767.0
            byteBuffer1[i] = (byte) x; // low byte
            byteBuffer1[++i] = (byte) (x >>> 8);  // high byte
        }


        byteBuffer2 = new byte[buffer2.length*2];

        for(int j = 0, bufferIndex=0; j < byteBuffer2.length; j++){
            y = (short) (buffer2[bufferIndex++]*32767.0);
            byteBuffer2[j] = (byte) y;         // low byte
            byteBuffer2[++j] = (byte) (y >>> 8);  // high byte

        }

        byteBufferFinal = new byte[byteBuffer1.length*2];
        //LL RR LL RR LL RR
        for(int k = 0, index = 0; index < byteBufferFinal.length - 4; k=k+2){
            byteBufferFinal[index] = byteBuffer1[k]; // LEFT {0,1/4,5/8,9/12,13;...}
            byteBufferFinal[index+1] = byteBuffer1[k+1];
            index = index + 2;
            byteBufferFinal[index] = byteBuffer2[k]; // RIGHT {2,3/6,7/10,11;...}
            byteBufferFinal[index+1] = byteBuffer2[k+1];
            index = index + 2;
        }

        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                (int) sampleRate,AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_16BIT,byteBufferFinal.length,
                AudioTrack.MODE_STATIC);

        audioTrack.write(byteBufferFinal, 0, byteBufferFinal.length);
        audioTrack.play();
    }


    Runnable r = new Runnable() {
        @Override
        public void run() {
            try {
                audioTestWav();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            }
        }
    };

    void audioTestWav() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String filePath = "/mnt/sdcard/Music/02e.wav";
        String filepath2 = "/mnt/sdcard/Music/01e.wav";

        String aeskey = "This is a key123";

        AES aes = new AES( aeskey.getBytes() );
        // We keep temporarily filePath globally as we have only two sample sounds now..
        if (filePath==null)
            return;

        int intSize = android.media.AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
                AudioFormat.ENCODING_PCM_16BIT);

        AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
                AudioFormat.ENCODING_PCM_16BIT, intSize, AudioTrack.MODE_STREAM);
        at.setStereoVolume(AudioTrack.getMaxVolume(), AudioTrack.getMaxVolume());


        if (at==null){
            Log.d("TCAudio", "audio track is not initialised ");
            return;
        }

        int count = 2*1024; // 512 kb

        byte[] byteData = null;
        File file = null;
        file = new File(filePath);

        byteData = new byte[(int)count];

        FileInputStream in = null;
        try {
            in = new FileInputStream( file );

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        File file2 = null;
        file2 = new File(filepath2);
        FileInputStream in2 = null;
        try {
            in2 = new FileInputStream( file2 );

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        int bytesread = 0, ret = 0;
        int size = (int) file.length();
        at.play();
        byte[] resBuffer = new byte[ byteData.length*2 ];
        Arrays.fill(resBuffer, (byte)0);
        int k = 0;

        float volume1 = 0.9f;
        float volume2 = 0.9f;


        while (bytesread < size)
        {
            try {
                ret = in.read( byteData,0, count);

                byteData = aes.decrypt(byteData);

                k = 0;
                for (int i = 0; i<ret; i+=2)
                {
                    short d = (short)( ((byteData[i+1]&0xFF)<<8) | (byteData[i]&0xFF) );
                    d *= volume1;
                    resBuffer[k] = (byte)(d&0xFF);
                    resBuffer[k+1] = (byte)(( d >> 8 ) & 0xFF);

                    k+= 4;
                }

                ret = in2.read( byteData,0, count);
                byteData = aes.decrypt(byteData);

                k = 2;
                for (int i = 0; i<ret; i+=2)
                {
                    short d = (short)( ((byteData[i+1]&0xFF)<<8) | (byteData[i]&0xFF) );
                    d *= volume1;
                    resBuffer[k] = (byte)(d&0xFF);
                    resBuffer[k+1] = (byte)(( d >> 8 ) & 0xFF);
                    k+= 4;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (ret != -1)
            {
                at.write(resBuffer,0, ret*2);
                bytesread += ret;
            }
            else break;
        }

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        at.stop();
        at.release();

    }


    public void onClick(View view) {

            Log.d( "TEST", aShoutPlayer.stringFromJNI() );

    }

    /**
     * Кнопка Стерео
     * @param view
     */
    public void click2(View view) {
        //testAudio(AudioFormat.CHANNEL_OUT_STEREO);
       // audioTestWav();


        Thread t = new Thread(r);
        t.start();
    }

    public void asyncTest(View view) {
        String filepath = "/mnt/sdcard/Music/02e.wav";
        String filepath2 = "/mnt/sdcard/Music/01e.wav";

        /*try {
            ShoutFile sf = new ShoutFile(filepath);
            NPLayer player = new NPLayer();
            for( int i=0; i<2; ++i ) {
                while (!sf.isEnd()) {
                    byte[] d = new byte[2 * 1024];
                    long t = sf.getBuffer(d, 2 * 1024);
                    player.prepareBuffer(d, t);
                    player.playBuffer();
                }
                sf.reset();
                Log.d("TEST", "End read file");
            }
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        NPLayer pl = null;
        NPLayer rl = null;
        try {
            pl = new NPLayer(filepath);
            rl = new NPLayer(filepath2);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        pl.setN(2);
        rl.setN(3);
        rl.setChannel(aShoutPlayer.CHANNEL_SELECT.RIGHT);
        pl.setChannel(aShoutPlayer.CHANNEL_SELECT.LEFT);

        Thread th1 = new Thread(pl);
        Thread th2 = new Thread(rl);

        th1.start();
        th2.start();
    }

    /**
     * Слушатель ответа
     */
    private class PopularProductsRequestListener implements RequestListener<ProductList>
    {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Log.w("aaa", "false");
        }

        @Override
        public void onRequestSuccess(ProductList products) {

            for(Product p: products)
            {
                Log.w("aaa", p.getName());
            }

        }
    }


    /**
     * Приемник запроса авторизации
     */
    private class AuthRequestListener implements RequestListener<UserTokenModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(UserTokenModel userTokenModel) {

        }
    }
}
