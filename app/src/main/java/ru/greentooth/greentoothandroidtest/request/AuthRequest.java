package ru.greentooth.greentoothandroidtest.request;
import android.net.Uri;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import ru.greentooth.greentoothandroidtest.model.UserTokenModel;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.http.MediaType;
import org.springframework.http.HttpEntity;

public class AuthRequest extends SpringAndroidSpiceRequest<UserTokenModel> {

    private String username = "";
    private String password = "";

    public AuthRequest(String username, String password)
    {
        super(UserTokenModel.class);
        if ( !username.isEmpty() && !password.isEmpty() )
        {
            this.username = username;
            this.password = password;
        }
    }

    @Override
    public UserTokenModel loadDataFromNetwork() throws Exception {
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
        parameters.set("username", this.username);
        parameters.set("password", this.password);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters, headers);

        return getRestTemplate().postForObject("http://10.0.2.2:8000/rest-auth/login/", request, UserTokenModel.class);

    }
}
