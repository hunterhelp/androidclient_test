package ru.greentooth.greentoothandroidtest.request;

import android.net.Uri;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ru.greentooth.greentoothandroidtest.model.ProductList;


public class PopularProductsRequest extends BaseClientRequest<ProductList> {

    public PopularProductsRequest()
    {
        super(ProductList.class);
    }

    @Override
    public ProductList loadDataFromNetwork() throws Exception {
        Uri.Builder uriBuilder = Uri.parse("http://10.0.2.2:8000/product/new/").buildUpon();
        String url = uriBuilder.build().toString();
        RestTemplate template = getRestTemplate();
        template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return template.getForObject(url, ProductList.class);
    }
}
