package ru.greentooth.greentoothandroidtest.request;

import com.google.api.client.json.Json;

import org.apache.commons.lang3.ObjectUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import java.io.IOException;


/**
 * Класс-обертка для кастомизации HTTP заголовков запросов
 */
class CustomAuthHeaders implements ClientHttpRequestInterceptor {

    protected static String authString = "";

    public CustomAuthHeaders() throws JSONException {
        if ( authString.isEmpty() ) {
            buildAuthString();
        }
    }

    /**
     * Генерация строки авторизации
     * @throws JSONException
     */
    private static void buildAuthString() throws JSONException {
        //TODO: тут надо забрать откуда-то авторизационные данные
        String userToken = "";
        String deviceToken = "";
        authString = new JSONObject().put("token", userToken).put("device", deviceToken).toString();
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);

        request.getHeaders().set("Authorization", authString);
        return execution.execute(requestWrapper, body);
    }
}
