package ru.greentooth.greentoothandroidtest.request;


import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.json.JSONException;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;


public abstract class BaseClientRequest<RESULT> extends SpringAndroidSpiceRequest<RESULT> {
    public BaseClientRequest(Class<RESULT> clazz) {
        super(clazz);
    }

    @Override
    public RestTemplate getRestTemplate() {
        RestTemplate template = super.getRestTemplate();
        ClientHttpRequestInterceptor interceptor = null;
        try {
            interceptor = new CustomAuthHeaders();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        template.setInterceptors(Collections.singletonList(interceptor));
        return template;
    }

    public RestTemplate getCleanTemplate()
    {
        return super.getRestTemplate();
    }
}
