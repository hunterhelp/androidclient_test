package ru.greentooth.greentoothandroidtest.request;

import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.apache.commons.lang3.ObjectUtils;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import ru.greentooth.greentoothandroidtest.model.LogoutModel;
import ru.greentooth.greentoothandroidtest.model.UserTokenModel;

public class LogoutRequest extends SpringAndroidSpiceRequest<LogoutModel> {

    public LogoutRequest() {
        super(LogoutModel.class);
    }

    @Override
    public LogoutModel loadDataFromNetwork() throws Exception {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();


        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters, headers);

        return getRestTemplate().postForObject("http://10.0.2.2:8000/rest-auth/logout/", request, LogoutModel.class);
    }
}
