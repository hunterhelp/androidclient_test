package ru.greentooth.greentoothandroidtest;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


/**
 * Абстрактный плеер криков
 */
abstract class aShoutPlayer implements Runnable
{
    static {
        System.loadLibrary("myapp");
    }

    protected AudioTrack track;
    protected ShoutFile file;

    protected Float volume;
    enum CHANNEL_SELECT {
        RIGHT,
        LEFT,
        BOTH};
    protected CHANNEL_SELECT channel; //канал
    private AES aes; //дешифрование
    protected byte[] audiobuffer; //аудиобуффер


    final static int SAMPLE_RATE = 44100;
    protected final static int AUDIO_READ_SIZE = 2*1024;

    public void setChannel(CHANNEL_SELECT channel) {
        this.channel = channel;
    }

    public void setVolume(Float volume) {
        synchronized (volume) {
            this.volume = volume;
        }
    }

    public static native String stringFromJNI();
    public aShoutPlayer(String filepath) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        super();
        file = new ShoutFile(filepath);
        volume = new Float(1);
        channel = CHANNEL_SELECT.LEFT;
        int buffersize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        track =  new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
                AudioFormat.ENCODING_PCM_16BIT, buffersize, AudioTrack.MODE_STREAM);
        track.play();
        // TODO: получить ключ шифрования
        String aeskey = stringFromJNI();

        aes = new AES( aeskey.getBytes() );

        audiobuffer = new byte[AUDIO_READ_SIZE*2];
        Arrays.fill(audiobuffer, (byte) 0);
    }


    public void playBuffer()
    {
        track.write(audiobuffer,0,audiobuffer.length);
    }


    /**
     * Подготоваливает буфер для передачи его на воспроизведение. Расшифровывает, выводит на канал
     * и заполняет аудиобуффер введенными данными
     * @param buffer
     */
    public void prepareBuffer(byte[] buffer, long size)
    {
        if ( buffer == null ) return;
        try {
            buffer = aes.decrypt(buffer);
            int k = ( channel == CHANNEL_SELECT.RIGHT) ? 2 : 0;
            short d;
            synchronized ( volume ) {
                for (int i = 0; i < size; i += 2, k += 4) {
                    d = (short) (((buffer[i + 1] & 0xFF) << 8) | (buffer[i] & 0xFF));

                    audiobuffer[k] =(byte)(d&0xFF);
                    audiobuffer[k + 1] = (byte)(( d >> 8 ) & 0xFF);

                    if ( channel == CHANNEL_SELECT.BOTH )
                    {
                        audiobuffer[k+2] = (byte)(d&0xFF);
                        audiobuffer[k+3] = (byte)(( d >> 8 ) & 0xFF);
                    }
                }
            }

        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

}

/**
 * Проигрыватель, который играет N раз указанный трек
 */
class NPLayer extends aShoutPlayer
{
    private int N = 0;

    public NPLayer(String filepath) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        super(filepath);
    }

    public void setN(int N)
    {
        N = (N<0)?0:N;
        this.N = N;
    }

    @Override
    public void run() {
        byte[] data = new byte[AUDIO_READ_SIZE];
        long rd = 0;
        for( int i = 0; i<this.N; i += this.N > 0 ? 1: 0 ,file.reset()  )
        {
            while( !file.isEnd() )
            {
                try {
                    rd = file.getBuffer(data, AUDIO_READ_SIZE);
                    prepareBuffer(data, rd);
                    playBuffer();
                } catch (IOException e) {
                    // TODO: бросить исключение дальше
                    Log.e("TEST","Exceptionss");
                    e.printStackTrace();
                }

            }
            Log.d("TEST", "New loop");
        }
    }

}

/**
 * Непрерывный проигрыватель
 */
class ContinuousShoutPlayer extends NPLayer
{
    public ContinuousShoutPlayer(String filepath) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        super(filepath);
        setN(-1);
    }
}

public class ShoutPlayer {

}
