package ru.greentooth.greentoothandroidtest;


import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

/***
 * Класс для работы с файлом крика
 */
public class ShoutFile {

    private long readed = 0; //сколько байт прочитано
    private long filesize = -1;
    private boolean fn = false;
    private File file;
    private FileInputStream fstream;


    /**
     * Конструктор
     * @param filepath путь до файла
     */
    public ShoutFile(String filepath) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        assert ( filepath.isEmpty() );

        this.file = new File(filepath);
        readed = 0;
        reset();
        getFilesize();
    }

    /**
     * Сброс на начало
     */
    public void reset()
    {
        if (file!=null)
        {
            try {
                if (fstream != null) fstream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                fstream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        readed = 0;
        fn = false;
    }

    /**
     * Размер файла
     * @return
     */
    public long getFilesize()
    {
        if( this.filesize <0 )
        {
            this.filesize = file.getTotalSpace();
        }
        return this.filesize;
    }

    /**
     * Количество прочитанных байт
     * @return количество прочитанных байт
     */
    public long getReaded() {
        return readed;
    }


    /**
     * Читаем буфер заданного размера из файла
     * @param buffer буффер для чтения
     * @param cnt количество байт, которое надо прочитать
     * @return количество прочитанных байт
     * @throws IOException
     */
    public long getBuffer(byte[] buffer, int cnt) throws IOException {
        long rd = 0;
        if( cnt>=0 ) {
            rd = fstream.read(buffer, 0, cnt);
        }
        this.readed += rd;
        if (rd<=0) {
            fn = true;
        }
        return rd;
    }

    public boolean isEnd()
    {
        return (this.readed >= this.filesize) || fn;
    }
}
