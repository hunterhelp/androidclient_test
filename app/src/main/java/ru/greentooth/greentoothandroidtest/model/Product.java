package ru.greentooth.greentoothandroidtest.model;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    private UUID _id;
    private String _name;
    private String _url;

    public UUID getId()
    {
        return _id;
    }

    public void setId(UUID id)
    {
        this._id = id;
    }

    public String getName()
    {
        return _name;
    }

    public void setName(String name)
    {
        this._name = name;
    }

    public void setUrl(String url)
    {
        this._url = url;
    }

    public String getUrl()
    {
        return this._url;
    }
}
