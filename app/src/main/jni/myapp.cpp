#include <android/log.h>
#include <string>
#include "myapp.h"



JNIEXPORT jstring JNICALL Java_ru_greentooth_greentoothandroidtest_aShoutPlayer_stringFromJNI(JNIEnv* env, jclass clazz)
{
    std::string data("Hello");
    return env->NewStringUTF(data.c_str());
}
